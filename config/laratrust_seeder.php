<?php

return [
    'role_structure' => [
        'admin' => [
            'users' => 'c,r_own,r_any,u_own,u_any,d_own,d_any',
            'faculty' => 'c,r_own,r_any,u_own,u_any,d_own,d_any'
        ],
        'user' => [
            'users' => 'r_own,u_own'
        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r_own' => 'read own',
        'r_any' => 'read any',
        'u_own' => 'update own',
        'u_any' => 'update any',
        'd_own' => 'delete own',
        'd_any' => 'delete any'
    ]
];
