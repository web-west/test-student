<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Faculty::class, function (Faker\Generator $faker) {
    return [
        'faculty' =>  $faker->word ,
    ];
});

$factory->define(App\Permission::class, function (Faker\Generator $faker) {
    return [
        'name' =>  $faker->name ,
        'display_name' =>  $faker->word ,
        'description' =>  $faker->word ,
    ];
});

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'first_name' =>  $faker->firstName ,
        'last_name' =>  $faker->lastName ,
        'sex' =>  $faker->word ,
        'birthday' =>  $faker->date() ,
        'group' =>  $faker->randomNumber() ,
        'faculty_id' =>  function () {
             return factory(App\Faculty::class)->create()->id;
        } ,
        'user_id' =>  function () {
             return factory(App\User::class)->create()->id;
        } ,
    ];
});

$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'name' =>  $faker->name ,
        'display_name' =>  $faker->word ,
        'description' =>  $faker->word ,
    ];
});

$factory->define(App\Group::class, function (Faker\Generator $faker) {
    return [
        'name' =>  $faker->name ,
        'display_name' =>  $faker->word ,
        'description' =>  $faker->word ,
    ];
});

