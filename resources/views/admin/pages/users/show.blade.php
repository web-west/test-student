@extends('admin.app')

@section('content')
    {{-- debug() --}}
    @if(count($user) > 0)
        <ul>
            <li><strong>Login: </strong>{{ $user->name }}</li>
            <li><strong>E-mail: </strong>{{ $user->email }}</li>
            <li><strong>Created: </strong>{{ $user->created_at }}</li>

            @if(!empty($user->profile->first_name))
                <li><strong>First name: </strong>{{ $user->profile->first_name }}</li>
            @endif

            @if(!empty($user->profile->first_name))
                <li><strong>Last name: </strong>{{ $user->profile->last_name }}</li>
            @endif

            @if(!empty($user->profile->first_name))
                <li><strong>Sex: </strong>{{ $user->profile->sex }}</li>
            @endif

            @if(!empty($user->profile->birthday))
                <li><strong>Birthday: </strong>{{ $user->profile->birthday }}</li>
            @endif

            @if(!empty($user->profile->group))
                <li><strong>Group: </strong>{{ $user->profile->group }}</li>
            @endif

            @if(!empty($user->profile->faculty->faculty))
                <li><strong>Faculty: </strong>{{ $user->profile->faculty->faculty }}</li>
            @endif
        </ul>

    @endif
@endsection