@extends('admin.app')

@section('content')
    {{-- debug($user->profile) --}}
    <form action="{{ route('users.update', $user->id) }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group input-group">
                    <span class="input-group-addon">Login</span>
                    <input type="text" class="form-control" required placeholder="Username" name="name" value="{{ $user->name }}">
                </div>

                <div class="form-group input-group">
                    <span class="input-group-addon">E-mail</span>
                    <input type="email" class="form-control" required placeholder="E-mail" name="email" value="{{ $user->email }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group input-group">
                    <span class="input-group-addon">Password</span>
                    <input type="password" class="form-control" name="password">
                </div>

                <div class="form-group input-group">
                    <span class="input-group-addon">Confirmed password</span>
                    <input type="password" class="form-control" name="confirmed">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group input-group">
                    <span class="input-group-addon">First name</span>
                    <input type="text" class="form-control" placeholder="First name" name="first_name" value="{{ $user->profile->first_name or '' }}">
                </div>

                <div class="form-group input-group">
                    <span class="input-group-addon">Last name</span>
                    <input type="text" class="form-control" placeholder="Last name" name="last_name" value="{{ $user->profile->last_name or '' }}">
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <select class="form-control" name="sex">
                        <option value="0">-- Select Sex --</option>
                        <option value="man" {{ !empty($user->profile->sex) && $user->profile->sex == 'man'  ? 'selected' : '' }}>Man</option>
                        <option value="woman" {{ !empty($user->profile->sex) && $user->profile->sex == 'woman'  ? 'selected' : '' }}>Woman</option>
                    </select>
                </div>

                <div class="form-group input-group">
                    <span class="input-group-addon">Birthday</span>
                    <input type="date" class="form-control" placeholder="Format: 2016-01-01" name="birthday" value="{{ $user->profile->birthday or '' }}">
                </div>
            </div>
            <div class="col-md-6">

                <div class="form-group input-group">
                    <span class="input-group-addon">Group</span>
                    <input type="number" class="form-control" name="group" value="{{ $user->profile->group or '' }}">
                </div>
            </div>
            @if(count($faculties) > 0)
            <div class="col-md-6">
                <div class="form-group">
                    <select class="form-control" name="faculty_id">
                        <option value="0">-- Select Faculty --</option>
                        @foreach($faculties as $faculty)
                        <option {{ !empty($user->profile->faculty_id) && $faculty->id == $user->profile->faculty_id ? 'selected' : '' }} value="{{ $faculty->id }}">{{ $faculty->faculty }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection