@extends('admin.app')

@section('content')
    <form action="{{ route('faculties.update', $faculty->id) }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group input-group">
                    <span class="input-group-addon">Faculty name</span>
                    <input type="text" class="form-control" required placeholder="Faculty name" name="name" value="{{ $faculty->faculty }}">
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection