@extends('admin.app')

@section('content')
    <form action="{{ route('faculties.store') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group input-group">
                    <span class="input-group-addon">Faculty name</span>
                    <input type="text" class="form-control" required placeholder="Faculty name" name="name">
                </div>
            </div>

        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
@endsection