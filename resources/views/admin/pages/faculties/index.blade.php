@extends('admin.app')

@push('styles')
<!-- DataTables CSS -->
<link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- DataTables Responsive CSS -->
<link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('faculties.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    @if(count($faculties) > 0)
    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr>
            <th>#</th>
            <th>Faculty name</th>
            <th>Created</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($faculties as $key => $faculty)
        <tr class="@if($key % 2) odd @else even @endif gradeX">
            <td>{{ $key + 1 }}</td>
            <td>{{ $faculty->faculty }}</td>
            <td>{{ $faculty->created_at }}</td>
            <td class="center">
                <a href="{{ route('faculties.edit', $faculty->id) }}" class="btn btn-primary btn-circle"><i class="fa fa-pencil"></i></a>
            </td>
            <td class="center">
                <form action="{{ route('faculties.destroy', $faculty->id) }}" method="POST" onsubmit="return confirm('Delete?')">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="btn btn-warning btn-circle"><i class="fa fa-times"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <!-- /.table-responsive -->
    @endif
@endsection

@push('scripts')
<!-- DataTables JavaScript -->
<script src="/sb-admin-2/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="/sb-admin-2/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="/sb-admin-2/vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
@endpush