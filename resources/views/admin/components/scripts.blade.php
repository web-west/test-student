<!-- jQuery -->
<script src="/sb-admin-2/vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/sb-admin-2/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/sb-admin-2/vendor/metisMenu/metisMenu.min.js"></script>

@stack('scripts')

<!-- Custom Theme JavaScript -->
<script src="/sb-admin-2/js/sb-admin-2.js"></script>