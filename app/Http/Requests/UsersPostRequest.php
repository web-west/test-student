<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'password' => 'min:6|confirmed',
            'group' => 'numeric|integer|min:1',
            'birthday' => 'date_format:Y-m-d',
            'sex' => 'max:5',
            'faculty_id' => 'exists:faculties,id',
        ];
    }
}
