<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function __construct()
    {
        $this->data = [];
        $this->data['title'] = 'Title';
        $this->data['panel_title'] = 'Title';
    }
}
