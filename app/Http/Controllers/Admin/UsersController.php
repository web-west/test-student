<?php

namespace App\Http\Controllers\Admin;

use App\Faculty;
use App\Http\Requests\UsersPostRequest;
use App\Http\Controllers\MainController;
use App\User;
use App\Profile;

class UsersController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'Users page';
        $this->data['panel_title'] = 'Table users items';
        $this->data['users'] = User::paginate(50);
        return view('admin.pages.users.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'Create user profile';
        $this->data['panel_title'] = 'Form create user profile';
        $this->data['faculties'] = Faculty::all();

        return view('admin.pages.users.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersPostRequest $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'name' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        $user = new User();

        $profile = new Profile;

        $profile->first_name = $request->first_name;
        $profile->last_name = $request->last_name;
        !empty($request->sex) ? $profile->sex = $request->sex : '';
        !empty($request->birthday) ? $profile->birthday = $request->birthday : '';
        !empty($request->group) ? $profile->group = $request->group : '';
        !empty($request->faculty_id) ? $profile->faculty_id = $request->faculty_id : '';

        !empty($request->password) ? $user->password = bcrypt($request->password) : '';
        $user->email = $request->email;
        $user->name = $request->name;
        $user->save();

        $user->profile()->save($profile);
        return redirect()->route('users.index')->with('success', 'User create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['title'] = 'User profile';
        $this->data['panel_title'] = 'Information user profile';
        $this->data['user'] = User::findOrFail($id);

        return view('admin.pages.users.show', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit user profile';
        $this->data['panel_title'] = 'Form edit user profile';
        $this->data['user'] = User::findOrFail($id);
        $this->data['faculties'] = Faculty::all();
        return view('admin.pages.users.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersPostRequest $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $id,
            'name' => 'required|unique:users,name,' . $id,
        ]);

        $user = User::find($id);

        $profile = $user->profile ?: new Profile;

        $profile->first_name = $request->first_name;
        $profile->last_name = $request->last_name;
        !empty($request->sex) ? $profile->sex = $request->sex : '';
        !empty($request->birthday) ? $profile->birthday = $request->birthday : '';
        !empty($request->group) ? $profile->group = $request->group : '';
        !empty($request->faculty_id) ? $profile->faculty_id = $request->faculty_id : '';

        !empty($request->password) ? $user->password = bcrypt($request->password) : '';

        $user->email = $request->email;
        $user->name = $request->name;
        $user->save();

        $user->profile()->save($profile);

        return redirect()->route('users.index')->with('success', 'User update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $profile = $user->profile;
        $profile->delete();
        $user->delete();
        return redirect()->route('users.index')->with('success', 'User delete');
    }
}
