<?php

namespace App\Http\Controllers\Admin;

use App\Faculty;
use App\Http\Controllers\MainController;
use App\Http\Requests\FacultiesPostRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FacultiesController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['title'] = 'Faculties page';
        $this->data['panel_title'] = 'Faculties items';
        $this->data['faculties'] = Faculty::paginate(50);
        return view('admin.pages.faculties.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = 'Create faculty';
        $this->data['panel_title'] = 'Form create faculty';
        return view('admin.pages.faculties.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FacultiesPostRequest $request)
    {
        $faculty = new Faculty();
        $faculty->faculty = $request->name;
        $faculty->created_at = Carbon::now();
        $faculty->save();

        return redirect()->route('faculties.index')->with('success', 'Faculty create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        return abort(404);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = 'Edit faculty';
        $this->data['panel_title'] = 'Form edit faculty';
        $this->data['faculty'] = Faculty::find($id);

        return view('admin.pages.faculties.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FacultiesPostRequest $request, $id)
    {
        $faculty = Faculty::findOrFail($id);
        $faculty->faculty = $request->name;

        $faculty->save();
        return redirect()->route('faculties.index')->with('success', 'Faculty update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faculty = Faculty::findOrFail($id);
        $faculty->delete();
        return redirect()->route('faculties.index')->with('success', 'Faculty delete');
    }
}
