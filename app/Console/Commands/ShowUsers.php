<?php

namespace App\Console\Commands;

use App\Profile;
use App\User;
use Illuminate\Console\Command;

class ShowUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all registered users';

    /**
     * The table headers for the command.
     *
     * @var array
     */
    protected $headers = [
        'Login',
        'Email',
        'First Name',
        'Last Name',
        'Sex',
        'Birthday',
        'Group',
        'Faculty'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->users = $this->getUsers();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (count($this->users) == 0) {
            return $this->error("Your application doesn't have any users.");
        }

        $this->displayUsers($this->getUsers());
    }

    /**
     * Compile the users into a displayable format.
     *
     * @return array
     */
    protected function getUsers()
    {

        $users = User::all();
        $results = [];

        foreach ($users as $user) {
            $results[] = [
                $user->name,
                $user->email,
                !empty($user->profile->first_name) ? $user->profile->first_name : 'null',
                !empty($user->profile->last_name) ? $user->profile->last_name : 'null',
                !empty($user->profile->sex) ? $user->profile->sex : 'null',
                !empty($user->profile->birthday) ? $user->profile->birthday : 'null',
                !empty($user->profile->group) ? $user->profile->group : 'null',
                !empty($user->profile->faculty->faculty) ? $user->profile->faculty->faculty : 'null',
            ];
        }

        return $results;

    }

    /**
     * Display the user information on the console.
     *
     * @param  array  $routes
     * @return void
     */
    protected function displayUsers(array $users)
    {
        //dd($users);
        $this->table($this->headers, $users);
    }
}
